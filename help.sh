#!/bin/sh

ID=~/.ssh/elzemieke
export SSH_ASKPASS=/run/current-system/sw/bin/ksshaskpass
# Use setsid to get ksshaskpass to pop up
setsid ssh -i $ID -N -R 1922:localhost:22 elzemieke@elzemieke.com -p 2222 &
PID=$!
zenity --info --text="Click OK after I've fixed all the things :D"
echo $PID
kill -9 $PID
