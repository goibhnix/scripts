#!/bin/sh

#TASK=`kdialog --menu "Select a task:" update "Update" help "Get help"`
TASK=`zenity --list --title "Select a task:" --column "" --column "Action" update "Update" help "Get help"`
SCRIPTSDIR=~/.config/goibhnix/scripts/

if [ "$TASK" = "update" ]
then
    echo update
    $SCRIPTSDIR/update.sh
elif [ "$TASK" = "help" ]
then
    echo help
    $SCRIPTSDIR/help.sh
fi
