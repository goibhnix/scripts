#!/bin/sh

CONFIGDIR=~/.config/goibhnix
#dbusRef=`kdialog --title "System upgrade" --progressbar "Upgrading configuration" 2`

function pull {
    cd $CONFIGDIR/scripts
    git pull

    if [ ! -d $CONFIGDIR/configurations ];
    then
	mkdir -p $CONFIGDIR
	cd $CONFIGDIR
	git clone https://gitlab.com/goibhnix/configurations.git
    else
	cd $CONFIGDIR/configurations
	git pull
    fi
}

#qdbus $dbusRef Set "" value 1
#qdbus $dbusRef setLabelText "Rebuilding system"

LOGDIR=/home/elzemieke/.log/goibhnix
mkdir -p $LOGDIR
DATE=$(date +%Y-%m-%d-%H%M)
LOGFILE=$LOGDIR/update_$DATE.log

function update {
    #kdesu -t nixos-rebuild switch --upgrade  > $LOGFILE 2>&1
    gksu "nixos-rebuild switch --upgrade"  > $LOGFILE 2>&1
}

(echo "50"; pull; echo "50"; update) | zenity --progress --text="Updating" --percentage=0 

zenity --info --text="System updated"
zenity --text-info --title="Please send me this log file by email:" --filename=$LOGFILE

# if [ $? -eq 0 ]
# then
#     # qdbus $dbusRef Set "" value 2
#     # qdbus $dbusRef setLabelText "System updated"
# else
#     # qdbus $dbusRef close
#     # kdialog --error 'Upgrade failed. Give it another shot, but if it keeps 
#     #                 failing send me an email'
    
# # with this error:\n\n $(cat $LOGFILE)'
# fi
